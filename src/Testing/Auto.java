package Testing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lab
 */
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lab
 */
public class Auto {
    
    private Engine engine;
    private String model;
    private String color;
    private int speed;

    public Auto(Engine engine, String model, 
            String color, int speed) {
        this.engine = engine;
        this.model = model;
        this.color = color;
        this.speed = speed;
    }

    
    public void speedUp(int speedIncrease){
        
        this.speed = this.speed + speedIncrease;
        this.engine.speedUp(speedIncrease);
    }
    
    public void slowDown(int speedDecrease){
        this.speed = this.speed - speedDecrease;
        this.engine.slowDown(speedDecrease);
    }
    
    public void printInfo(){
        
        System.out.println("======================");
        System.out.println("Model: " + this.model);
        this.engine.printInfo();
        System.out.println("Color: " + this.color);
        System.out.println("Speed: " + this.speed);
        System.out.println("======================");
        
    }

    @Override
    public String toString() {
        return "Auto{" + "engine=" + engine + ", model=" + model + ", color=" + color + ", speed=" + speed + '}';
    }
    
}

