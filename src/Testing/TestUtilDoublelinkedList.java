package Testing;

import weiss.util.LinkedList;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ehazizi
 */
public class TestUtilDoublelinkedList {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Auto a1 = new Auto(null, "W", "red", 110);
        Auto a2 = new Auto(null, "B", "red", 120);
        Auto a3 = new Auto(null, "M", "red", 130);
        Auto a4 = new Auto(null, "F", "yellow", 140);
        LinkedList<Auto> myAutos = new LinkedList<Auto>();
        myAutos.addFirst(a1);
        myAutos.addFirst(a2);
        myAutos.addFirst(a3);
        myAutos.addFirst(a4);
        myAutos.print();
        System.out.println("after substituting");
        AutoComparator<Auto> cmp = new AutoComparator<Auto>();
        myAutos.substitute(a1, a4, cmp);
        myAutos.print();
        myAutos.remove(1, 2);
        System.out.println("after removal");
        myAutos.print();
        Auto a8 =  new Auto(null, "F", "yellow", 140);
        Auto a5 = new Auto(null, "A", "red", 120);
        Auto a6 = new Auto(null, "B", "red", 130);
        Auto a7 = new Auto(null, "C", "yellow", 140);
        LinkedList<Auto> myAutosNew = new LinkedList<Auto>();
        myAutosNew.add(a7);
        myAutosNew.add(a8);
        myAutosNew.add(a5);
        myAutosNew.add(a6);
         System.out.println("new list of Autos");
        myAutosNew.print();
        LinkedList<Auto> intersectedList=myAutosNew.intersect(myAutos, myAutosNew, cmp);
        System.out.println("Intersection of Autos");
        intersectedList.print();
    }

}
