package Testing;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Lab
 */
public class Engine {
    
    private String model;
    private double volume;
    private int maxrpm;
    private int rpm;

    public Engine(String model, double volume, 
            int maxrpm, int rpm) {
        this.model = model;
        this.volume = volume;
        this.maxrpm = maxrpm;
        this.rpm = rpm;
    }

    public void printInfo(){
        
        System.out.println("Engine model: " + model);
        System.out.println("Engine volume: " + volume);
        System.out.println("Engine maxrpm: " + maxrpm);
        System.out.println("Engine current rpm: " + rpm);
        
    }
     
    public void speedUp(int speed){ 
        int increaseRpm = speed * 10;
        this.rpm = this.rpm + increaseRpm;  
    }
    
    public void slowDown(int speed){ 
        int decreaseRpm = speed * 10;
        this.rpm = this.rpm - decreaseRpm;  
    }
    
}
