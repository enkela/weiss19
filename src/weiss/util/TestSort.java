/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weiss.util;

/**
 *
 * @author CS-4
 */
// Test all the sorting routines in package DataStructures

import weiss.util.Random;

public class TestSort
{
    private static final int NUM_ITEMS = 1000;
    private static int theSeed = 1;
    
    /**
     * Randomly rearrange an array.
     * The random numbers used depend on the time and day.
     * @param a the array.
     */
    public static final <AnyType> void permute( AnyType [ ] a )
    {
        Random r = new Random( );

        for( int j = 1; j < a.length; j++ )
            Sort.swapReferences( a, j, r.nextInt( 0, j ) );
    }

    public static void checkSort( Integer [ ] a )
    {
        for( int i = 0; i < a.length; i++ )
            if( a[ i ] != i )
                System.out.println( "Error at " + i );
        System.out.println( "Finished checksort" );
    }


    public static void main( String [ ] args )
    {
        Integer [ ] a = new Integer[ NUM_ITEMS ];
        Integer [ ] b = {81,94,11,96,12,35,17,95,28,58,41,75,15};
        Integer [ ] b1 = {81,94,11,96,12,35,17,95,28,58,41,75,15};
        Integer [ ] b2 = {81,94,11,96,12,35,17,95,28,58,41,75,15};
        Integer [ ] b3 = {81,94,11,96,12,35,17,95,28,58,41,75,15};
        Integer [ ] c = new Integer[ NUM_ITEMS ];
        for( int i = 0; i < a.length; i++ )
            a[ i ] = i;
//       
         Sort.shellsort(b);
         Sort.shellsort1(b1);
           Sort.insertionSort(b2);
           Sort.mergeSort(b3);
//        for( theSeed = 0; theSeed < 20; theSeed++ )
//        {
//            permute( a );
//            Sort.insertionSort( a );
//            checkSort( a );
//
//            permute( a );
//            Sort.heapsort( a );
//            checkSort( a );
//
//            permute( a );
//            Sort.shellsort( a );
//            checkSort( a );
//
//            permute( a );
//            Sort.mergeSort( a );
//            checkSort( a );
//
//            permute( a );
//            Sort.quicksort( a );
//            checkSort( a );
//
//            permute( a );
//            Sort.quickSelect( a, NUM_ITEMS / 2 );
//            System.out.println( a[ NUM_ITEMS / 2 - 1 ] + " is " + NUM_ITEMS / 2 +"th smallest" );
//        }
    }
}
