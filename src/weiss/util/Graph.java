/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weiss.util;                                    
// Represents a vertex in the graph.

import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;
import weiss.nonstandard.PairingHeap;// Represents an entry in the priority queue for Dijkstra's algorithm.

class GraphException extends RuntimeException
{       
    public GraphException( String name )
    {
        super( name );
    }
}
class Path implements Comparable<Path> {

    public Vertex dest;   // w
    public double cost;   // d(w)

    public Path(Vertex d, double c) {
        dest = d;
        cost = c;
    }

    public int compareTo(Path rhs) {
        double otherCost = rhs.cost;

        return cost < otherCost ? -1 : cost > otherCost ? 1 : 0;
    }
}
// Represents an edge in the graph.

class Edge {

    public Vertex dest;   // Second vertex in Edge
    public double cost;   // Edge cost

    public Edge(Vertex d, double c) {
        dest = d;
        cost = c;
    }
}

class Vertex {

    public String name;   // Vertex name
    public List<Edge> adj;    // Adjacent vertices
    public double dist;   // Cost
    public Vertex prev;   // Previous vertex on shortest path
    public int scratch;// Extra variable used in algorithm

    public Vertex(String nm) {
        name = nm;
        adj = new LinkedList<Edge>();
        reset();
    }

    public void reset() {
        dist = Graph.INFINITY;
        prev = null;
        pos = null;
        scratch = 0;
    }

    public PairingHeap.Position<Path> pos;  // Used for dijkstra2 (Chapter 23)
}

/**
 *
 * @author CS
 */
public class Graph {

    public static final double INFINITY = Double.MAX_VALUE;
    private java.util.Map<String, Vertex> vertexMap = new java.util.HashMap<String, Vertex>();

    /**
     * Add a new edge to the graph.
     */
    public void addEdge(String sourceName, String destName, double cost) {
        Vertex v = getVertex(sourceName);
        Vertex w = getVertex(destName);
        v.adj.add(new Edge(w, cost));
    }

    /**
     * Driver routine to handle unreachables and print total cost. It calls
     * recursive routine to print shortest path to destNode after a shortest
     * path algorithm has run.
     */
    public void printPath(String destName) {
        Vertex w = vertexMap.get(destName);
        if (w == null) {
            throw new java.util.NoSuchElementException("Destination vertex not found");
        } else if (w.dist == INFINITY) {
            System.out.println(destName + " is unreachable");
        } else {
            System.out.print("(Cost is: " + w.dist + ") ");
            printPath(w);
            System.out.println();
        }
    }

    /**
     * If vertexName is not present, add it to vertexMap. In either case, return
     * the Vertex.
     */
    private Vertex getVertex(String vertexName) {
        Vertex v = vertexMap.get(vertexName);
        if (v == null) {
            v = new Vertex(vertexName);
            vertexMap.put(vertexName, v);
        }
        return v;
    }

    /**
     * Recursive routine to print shortest path to dest after running shortest
     * path algorithm. The path is known to exist.
     */
    private void printPath(Vertex dest) {
        if (dest.prev != null) {
            printPath(dest.prev);
            System.out.print(" to ");
        }
        System.out.print(dest.name);
    }

    /**
     * Initializes the vertex output info prior to running any shortest path
     * algorithm.
     */
    private void clearAll() {
        for (Vertex v : vertexMap.values()) {
            v.reset();
        }
    }
    
     public void dijkstra( String startName )
    {
        java.util.PriorityQueue<Path> pq = new java.util.PriorityQueue<Path>( );

        Vertex start = vertexMap.get( startName );
        if( start == null )
            throw new java.util.NoSuchElementException( "Start vertex not found" );

        clearAll( );
        pq.add( new Path( start, 0 ) ); start.dist = 0;
        
        int nodesSeen = 0;
        while( !pq.isEmpty( ) && nodesSeen < vertexMap.size( ) )
        {
            Path vrec = pq.remove( );
            Vertex v = vrec.dest;
            if( v.scratch != 0 )  // already processed v
                continue;
                
            v.scratch = 1;
            nodesSeen++;

            for( Edge e : v.adj )
            {
                Vertex w = e.dest;
                double cvw = e.cost;
                
                if( cvw < 0 )
                    throw new GraphException( "Graph has negative edges" );
                    
                if( w.dist > v.dist + cvw )
                {
                    w.dist = v.dist +cvw;
                    w.prev = v;
                    pq.add( new Path( w, w.dist ) );
                }
            }
        }
    }

    public void unweighted(String startName) {
        clearAll();

        Vertex start = vertexMap.get(startName);
        if (start == null) {
            throw new java.util.NoSuchElementException("Start vertex not found");
        }

        java.util.Queue<Vertex> q = new java.util.LinkedList<Vertex>();
        q.add(start);
        start.dist = 0;

        while (!q.isEmpty()) {
            Vertex v = q.remove();
            System.out.println("vertex: "+v.name);

            for (Edge e : v.adj) {
                Vertex w = e.dest;
                if (w.dist == INFINITY) {
                    w.dist = v.dist + 1;
                    w.prev = v;
                    q.add(w);
                }
            }
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here. Starting point is A input as in fig 14.5
        Graph g = new Graph();
        try {
            FileReader fin = new FileReader("Graph.txt");
            Scanner graphFile = new Scanner(fin);

            // Read the edges and insert
            String line;
            System.out.println("Print the Verytex map that is created");
            while (graphFile.hasNextLine()) {
                line = graphFile.nextLine();
                StringTokenizer st = new StringTokenizer(line);
                try {
                    if (st.countTokens() != 3) {
                        System.err.println("Skipping ill-formatted line " + line);
                        continue;
                    }
                    String source = st.nextToken();
                    String dest = st.nextToken();
                    int cost = Integer.parseInt(st.nextToken());
                    System.out.println("source, dest, cost:"+source+" "+ dest+" "+ cost);
                    g.addEdge(source, dest, cost);
                } catch (NumberFormatException e) {
                    System.err.println("Skipping ill-formatted line " + line);
                }
            }
        } catch (IOException e) {
            System.err.println(e);
        }
        System.out.println("File read...");
        System.out.println(g.vertexMap.size() + " vertices");
        Scanner in = new Scanner(System.in);
        while (processRequest(in, g))
             ;
    }

    /**
     * Process a request; return false if end of file.
     */
    public static boolean processRequest(Scanner in, Graph g) {
        try {
            System.out.print("Enter start node:");
            String startName = in.nextLine();

            System.out.print("Enter destination node:");
            String destName = in.nextLine();
//
//            System.out.print( "Enter algorithm (u, d, n, a ): " );
//            String alg = in.nextLine( );

            g.unweighted(startName);
            g.dijkstra(startName);
            g.printPath(destName);
        } catch (java.util.NoSuchElementException e) {
            return false;
        }
        return true;
    }
}
