package weiss.nonstandard;

// Basic node stored in unbalanced binary search trees
// Note that this class is not accessible outside
// of this package.

class BinaryNode_old<AnyType>
{
        // Constructor
    BinaryNode_old( AnyType theElement )
    {
        element = theElement;
        left = right = null;
    }

      // Data; accessible by other package routines
    AnyType             element;  // The data in the node
    BinaryNode_old<AnyType> left;     // Left child
    BinaryNode_old<AnyType> right;    // Right child
}
