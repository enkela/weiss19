/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package weiss.nonstandard;

/**
 *
 * @author CS-4
 */
public class PrintBinarySearch {
    
      
static final int COUNT = 10;  
  
// A binary tree node  
//static class Node  
//{  
//    int data;  
//    Node left, right;  
//      
//    /* Constructor that allocates a new node with the  
//    given data and null left and right pointers. */
//    Node(int data) 
//    {  
//        this.data = data;  
//        this.left = null;  
//        this.right = null;  
//    }  
//};  
  
// Function to print binary tree in 2D  
// It does reverse inorder traversal  
static void print2DUtil(BinaryNode root, int space)  
{  
    // Base case  
    if (root == null)  
        return;  
  
    // Increase distance between levels  
    space += COUNT;  
  
    // Process right child first  
    print2DUtil(root.right, space);  
  
    // Print current node after space  
    // count  
//    System.out.print("\n");  
    for (int i = COUNT; i < space; i++)  
        System.out.print(" ");  
    System.out.print(root.element + "\n");  
  
    // Process left child  
    print2DUtil(root.left, space);  
}  
  
// Wrapper over print2DUtil()  
static void print2D(BinaryNode root)  
{  
    // Pass initial space count as 0  
    print2DUtil(root, 0);  
}  
  
// Driver code  
public static void main(String args[])  
{  
    BinaryNode root = new BinaryNode(1);  
    root.left = new BinaryNode(2);  
    root.right = new BinaryNode(3);  
  
    root.left.left = new BinaryNode(4);  
    root.left.right = new BinaryNode(5);  
    root.right.left = new BinaryNode(6);  
    root.right.right = new BinaryNode(7);  
  
    root.left.left.left = new BinaryNode(8);  
    root.left.left.right = new BinaryNode(9);  
    root.left.right.left = new BinaryNode(10);  
    root.left.right.right = new BinaryNode(11);  
    root.right.left.left = new BinaryNode(12);  
    root.right.left.right = new BinaryNode(13);  
    root.right.right.left = new BinaryNode(14);  
    root.right.right.right = new BinaryNode(15);  
    root.printInOrder();
    System.out.println("");
    root.printPreOrder();
    System.out.println("");
    root.printPostOrder();
//    BinarySearchTree<int> binarySearchTree = new BinarySearchTree<int>();
    print2D(root);  
}  
} 
  
    

